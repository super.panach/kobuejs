FROM node:alpine as base
WORKDIR /kobu-docker
COPY package*.json ./
RUN rm -rf node_modules && npm install --frozen-lockfile && npm cache clean --force
COPY . .
EXPOSE 6868
CMD npm start