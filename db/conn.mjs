require('dotenv').config({ path: './config.env' });

var { MongoClient } = require("mongodb");
var dbo = undefined;
const connectionString = process.env.ATLAS_URI;

export default function connexion() {
  return new MongoClient(connectionString, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }).connect(function (err, db) {
    if(err) throw err;
    dbo = db.db("kobu");
    console.log("Successfully connected to MongoDB.");
  });
}

/*let dbConnection;

module.exports = {
  connectToServer: function (callback) {
    client.connect(function (err, db) {
      if (err || !db) {
        console.log("Error connected to MongoDB.");
        return callback(err);
      }

      dbConnection = db.db("kobu");
      console.log("Successfully connected to MongoDB.");

      return callback();
    });
  },

  getDb: function () {
    return dbConnection;
  },
};*/