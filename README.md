**API pour l'application Kobu**

**Introduction**

Cette API tourne avec le framework ExpressJS 4.0 et 
afin de faciliter le fonctionnement de l'application, le dossier contenant 
l'api a été mis dans un container de docker. De plus, ce container est lancé 
à partir d'une image (préalablement construite) depuis l'application Docker
Desktop. 

**Prérequis pour l'utilisation de l'API**

Docker
- Docker Desktop (https://www.docker.com/products/docker-desktop) (je crois 
qu'il installe en même temps tout ce qui est nécessaire pour docker en ligne 
de commande)

Mongo DB
- L'adresse IP doit être accepté dans mongo db en ligne 
(https://www.mongodb.com/fr-fr) (normalement pas besoin d'en ajouter)

**Préambule**

Lancer la commande npm i pour installer les node_modules (il ne se trouve pas dans le git). Ensuite
pour lancer la construction ou la modification du docker, il est nécessaire de lancer cette ligne :
docker compose up --build

**Visualisation de l'API**

Pour visualiser l'API il y a deux solutions:
- Soit se rendre directement sur http://localhost:3000/
- Soit lancer le lien internet depuis docker Desktop

Si jamais Docker ne fonctionne pas lancer la commande npm start et se rendre sur le lien donné ci-
dessus.


**Example d'URL**

- Index : http://localhost:3000/
- User : http://localhost:3000/user/623c850eedc7f3fc81c10511/
- Feed : http://localhost:3000/feeds
- Article : http://localhost:3000/articles
- Quizz : http://localhost:3000/quizz
- Settings : http://localhost:3000/settings

**Section DEBUG**

Pour lancer l'API sans docker :
- npm start
- npm run devStart
- *DEBUG=kobuEjs:** *npm start*
- *DEBUG=kobuEjs:** *npm run devStart*

**Package**

Jade n'est plus utilisé et il recommande d'utiliser plutôt pug 

**Source**

Docker avec mongodb : https://www.bezkoder.com/docker-compose-nodejs-mongodb/
(Sinon voir sur docker hub avec mongodb)