var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/get/index');
var userRouter = require('./routes/get/user');
var commentsRouter = require('./routes/get/comment');
var feedsRouter = require('./routes/get/feeds');
var articlesRouter = require('./routes/get/articles');
var detailRouter = require('./routes/get/detail'); 
var quizzRouter = require('./routes/get/quizz'); 
var settingsRouter = require('./routes/get/settings'); 

var registrationRouter = require('./routes/post/registration');
var checkConnectionRouter = require("./routes/post/checkConnection");
var updateUserRouter = require("./routes/post/updateUser");

var app = express();

// view engine setup
app.set('view engine', 'jade');
app.set('views', path.join(__dirname, 'views'));

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser()); 
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/user', userRouter);
app.use('/feeds', commentsRouter);
app.use('/feeds', feedsRouter);
app.use('/articles', articlesRouter);
app.use('/articles/', detailRouter);
app.use('/quizz', quizzRouter);
app.use('/settings', settingsRouter);

app.use('/registration/', registrationRouter);
app.use('/checkConnection/', checkConnectionRouter);
app.use('/updateUser/', updateUserRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;