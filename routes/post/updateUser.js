const MongoClient = require('mongodb').MongoClient
require('dotenv').config({ path: './config.env' });

var express = require("express");
const { ObjectId } = require('mongodb');
var router = express.Router();
const connectionString = process.env.ATLAS_URI;

MongoClient.connect(connectionString, (err, client) => {
  if (err) throw err
  const db = client.db('kobu');
  router.get('/p', (req, res) => {
    db.collection('user').findOneAndUpdate(
        { _id: new ObjectId(req.query.id) }, 
        { $set: {
            xp: parseInt(req.query.xp),
            lvl: parseInt(req.query.lvl)
        }}, 
        { returnDocument: 'after' }, 
        (err, result) => {
            if (err) throw err
            res.send(result['value']);
        }
    )
  });
  
})

module.exports = router;