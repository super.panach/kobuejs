const MongoClient = require('mongodb').MongoClient
var express = require("express");
var router = express.Router();

require('dotenv').config({ path: './config.env' });
const connectionString = process.env.ATLAS_URI;

MongoClient.connect(connectionString, (err, client) => {
  if (err) throw err
  const db = client.db('kobu')
  router.get('/p', (req, res) => {
    var query = { 
      pseudo: req.query.pseudo, 
      password: req.query.password, 
      email: req.query.email, 
      name: req.query.name,
      image: null,
      xp: 1,
      lvl: 1
    };

    db.collection("user").insertOne(query, (err, result) => {
      if (err) throw err;
      console.log("1 document inserted");
      res.send(result);
    });
  })
});

module.exports = router;