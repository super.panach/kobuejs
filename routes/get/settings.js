const MongoClient = require('mongodb').MongoClient
var express = require("express");
var router = express.Router();

require('dotenv').config({ path: './config.env' });
const connectionString = process.env.ATLAS_URI;

MongoClient.connect(connectionString, (err, client) => {
  if (err) throw err
  const db = client.db('kobu')

  router.get('/', (req, res) => {
    db.collection('settings').find().toArray((err, result) => {
        if (err) throw err
        res.send(result)
    })
  });
  
})

module.exports = router;